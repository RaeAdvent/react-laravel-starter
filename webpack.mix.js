const mix = require('laravel-mix')

// absolute import
mix.webpackConfig({
  resolve: {
    modules: ['resources/js', 'node_modules'],
  },
})

// dynamic imports
// mix.babelConfig({
//   plugins: ['@babel/plugin-syntax-dynamic-import'],
// })

mix
  .react('resources/js/app.js', 'public/js')
  .postCss('resources/css/app.css', 'public/css', [require('tailwindcss')])
  .extract(['react', 'lodash'])
// .version()
