import React from 'react'

export default function Card({ title = '', img, children }) {
  return (
    <div className="rounded overflow-hidden shadow-lg m-3 hover:border-blue-300 border">
      {img && <img className="w-full" src={img} alt={title}></img>}
      <div className="px-6 py-4">
        <div className="font-bold text-xl mb-2">{title}</div>
        {children}
      </div>
    </div>
  )
}
