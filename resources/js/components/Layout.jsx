import React from 'react'

import PageTransition from 'components/PageTransition'
import Pages from 'routes/Pages'
import Navigation from './Navigation'

function Layout({ children }) {
  return (
    <div className="h-screen">
      <div className="w-full fixed">
        <Navigation />
      </div>
      <div className="h-full w-full flex flex-col items-center justify-center bg-gray-100">
        <div className="h-full p-5 w-11/12 bg-white pt-16">
          <PageTransition>
            <Pages />
          </PageTransition>
        </div>
      </div>
    </div>
  )
}

export default Layout
