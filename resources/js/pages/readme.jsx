import React from 'react'
import { Link } from '@reach/router'
import packageJson from '../../../package.json'
import composerJson from '../../../composer.json'

const JsPackages = () => {
  const { devDependencies, dependencies } = packageJson
  return (
    <div>
      <div className="italic font-bold">NPM</div>
      <div>
        <div className="underline my-3">Development Packages</div>
        <ul className="ml-3">
          {Object.keys(devDependencies).map(dev => (
            <li key={dev}>{`${dev} : ${devDependencies[dev]}`}</li>
          ))}
        </ul>
      </div>
      <div>
        <div className="underline my-3">Production Packages</div>
        <ul className="ml-3">
          {Object.keys(dependencies).map(p => (
            <li key={p}>{`${p} : ${dependencies[p]}`}</li>
          ))}
        </ul>
      </div>
    </div>
  )
}

const PhpPackages = () => (
  <div>
    <div className="italic font-bold">COMPOSER</div>
    <div>
      <div className="underline my-3">Development Packages</div>
      <ul className="ml-3">
        {Object.keys(composerJson['require-dev']).map(dev => (
          <li key={dev}>{`${dev} : ${composerJson['require-dev'][dev]}`}</li>
        ))}
      </ul>
    </div>
    <div>
      <div className="underline my-3">Production Packages</div>
      <ul className="ml-3">
        {Object.keys(composerJson['require']).map(p => (
          <li key={p}>{`${p} : ${composerJson['require'][p]}`}</li>
        ))}
      </ul>
    </div>
  </div>
)

const readme = props => {
  return (
    <>
      <div className="text-lg font-bold text-center">Readme</div>
      <div className="italic font-bold text-center">Installed Packages</div>
      <div className="flex flex-row justify-between px-10">
        <JsPackages />
        <PhpPackages />
      </div>
    </>
  )
}

export default readme
