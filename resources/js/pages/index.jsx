import React from 'react'
import Card from 'components/UI/Card'
import EaseIn from 'components/animations/EaseIn'
import { Link } from '@reach/router'
import packageJson from '../../../package.json'
import composerJson from '../../../composer.json'

const CardsDisplay = () => (
  <Card title="React - Laravel Starter Pack V2">
    <div className="italic">
      A simple starter pack for using Laravel framework and React.js
    </div>
  </Card>
)

const GettingStarted = () => {
  const { devDependencies } = packageJson

  return (
    <Card title="Getting Started">
      <div className="font-mono">
        <div>
          Go to{' '}
          <Link className="font-bold underline" to="/readme">
            Readme
          </Link>{' '}
          for installed npm or composer packages
        </div>

        <div className="italic font-mono mt-5">Specifications :</div>
        <div>
          Server : PHP - Laravel {composerJson.require['laravel/framework']}
        </div>
        <div>Frontend : Javascript - React.js {devDependencies.react}</div>
      </div>
    </Card>
  )
}

const index = props => {
  return (
    <div className="gap-5 flex flex-col justify-center">
      <EaseIn>
        <CardsDisplay />
        <GettingStarted />
      </EaseIn>
    </div>
  )
}

export default index
