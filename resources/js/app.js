import React from 'react';
import ReactDOM from 'react-dom';

import Layout from 'components/Layout'


function App() {
    return (
      <Layout/>
    );
}

export default App;

if (document.getElementById('app')) {
    ReactDOM.render(<App />, document.getElementById('app'));
}
