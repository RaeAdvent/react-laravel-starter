import React from 'react'
import { Router } from '@reach/router'

import Home from 'pages/index'
import Readme from 'pages/readme'

const Pages = ({ basepath = '/', ...props }) => {
  return (
    <Router basepath={basepath}>
      <Home path="/" />
      <Readme path="/readme" />
    </Router>
  )
}

export default Pages
