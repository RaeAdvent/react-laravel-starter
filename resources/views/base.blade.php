<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

      <title>{{ env('APP_NAME') }}</title>

      <link rel="shortcut icon" href="{{ asset('images/FL/FLW.png') }}" type="image/x-icon">
      <link rel="stylesheet" type="text/css" href="{{ mix('/css/app.css') }}">
      <!-- <link rel="stylesheet" type="text/css" href="{{ asset('/fonts/fonts.css') }}"> -->
    </head>
    <body cz-shortcut-listen="true" >
        <div id="app"  class="bg-gray-500"></div>
    </body>
  <script src="{{ mix('/js/manifest.js') }}" type="text/javascript"></script>
  <script src="{{ mix('/js/vendor.js') }}" type="text/javascript"></script>
  <script src="{{ mix('/js/app.js') }}" type="text/javascript"></script>
</html>
